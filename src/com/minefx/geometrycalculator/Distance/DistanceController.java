package com.minefx.geometrycalculator.Distance;

import com.minefx.geometrycalculator.Main;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class DistanceController implements Initializable {
    public Text ver;
    public javafx.scene.control.TextField xOne;
    public javafx.scene.control.TextField xTwo;
    public javafx.scene.control.TextField yOne;

    public TextField yTwo;
    public Text solution;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ver.setText("Version: " + Main.version);
    }
    public void onCalculate(ActionEvent actionEvent) throws Exception {
        if (xOne.equals(null)) {
            Alert error = new Alert(Alert.AlertType.ERROR, "You must specify a value for x1");
            error.showAndWait();
        } else if (xTwo.equals(null)) {
            Alert error = new Alert(Alert.AlertType.ERROR, "You must specify a value for x2");
            error.showAndWait();
        } else if (yOne.equals(null)) {
            Alert error = new Alert(Alert.AlertType.ERROR, "You must specify a value for y1");
            error.showAndWait();
        } else if (yTwo.equals(null)) {
            Alert error = new Alert(Alert.AlertType.ERROR, "You must specify a value for y2");
            error.showAndWait();
        } else {
            Double x = Double.parseDouble(xTwo.getText())-Double.parseDouble(xOne.getText().toString());
            Double xSquared = x * x;
            Double y = Double.parseDouble(yTwo.getText())-Double.parseDouble(yOne.getText());
            Double ySquared = y * y;
            Double sumXy = xSquared+ySquared;
            Double sqrtXy = Math.sqrt(sumXy);
            solution.setText(sqrtXy.toString());
        }
    }

}
