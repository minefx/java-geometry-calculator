package com.minefx.geometrycalculator.Distance;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Distance {
    public static void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Distance.class.getResource("../fxml/Distance.fxml"));
        primaryStage.setTitle("Distance");
        primaryStage.setScene(new Scene(root, 443, 326));
        primaryStage.show();
    }
}
