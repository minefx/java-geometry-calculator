package com.minefx.geometrycalculator.MainScreen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

public class MainScreen extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/MainScreen.fxml"));
        primaryStage.setTitle("Geometry Calculator");
        primaryStage.setScene(new Scene(root, 443, 326));
        primaryStage.show();
    }

    public static void load(String[] args) {
        launch(args);
    }
}
