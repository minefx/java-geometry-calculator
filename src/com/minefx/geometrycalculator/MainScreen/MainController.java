package com.minefx.geometrycalculator.MainScreen;

import com.minefx.geometrycalculator.Distance.Distance;
import com.minefx.geometrycalculator.Main;
import com.minefx.geometrycalculator.Midpoint.Midpoint;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    public Text ver;
    ObservableList list = FXCollections.observableArrayList();
    public ChoiceBox<String> calculatorFunctions;


    public void nextScreen(ActionEvent actionEvent) throws Exception {
        if (calculatorFunctions.getValue().equals("Distance")) {
            Distance.start(new Stage());
        } else if (calculatorFunctions.getValue().equals("Midpoint")) {
            Midpoint.start();
        }
    }
    public void loadList() {
        list.add("Distance");
        list.add("Midpoint");
        calculatorFunctions.getItems().addAll(list);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadList();
        ver.setText("Version: " + Main.version);
    }
}
