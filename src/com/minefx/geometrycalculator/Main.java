package com.minefx.geometrycalculator;

import com.minefx.geometrycalculator.MainScreen.MainScreen;

public class Main {
    public static String version = "41ec4bd";
    public static void main(String[] args) {
        MainScreen.load(args);
    }
}
