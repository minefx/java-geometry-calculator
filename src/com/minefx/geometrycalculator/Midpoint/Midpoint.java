package com.minefx.geometrycalculator.Midpoint;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Midpoint {
    public static void start() throws IOException {
        Parent root = FXMLLoader.load(Midpoint.class.getResource("../fxml/Midpoint.fxml"));
        Stage primaryStage = new Stage();
        primaryStage.setTitle("Midpoint");
        primaryStage.setScene(new Scene(root, 443, 326));
        primaryStage.show();
    }
}
