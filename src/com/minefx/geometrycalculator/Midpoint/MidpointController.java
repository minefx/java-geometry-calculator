package com.minefx.geometrycalculator.Midpoint;

import com.minefx.geometrycalculator.Main;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class MidpointController implements Initializable {
    public Text ver;
    public TextField xOne;
    public TextField xTwo;
    public TextField yOne;
    public TextField yTwo;
    public Text solution;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ver.setText("Version: " + Main.version);
    }
    public void onCalculate(ActionEvent actionEvent) {
        Double x = Double.parseDouble(xTwo.getText()) + Double.parseDouble(xOne.getText());
        Double halfX = x/2;
        Double y = Double.parseDouble(yTwo.getText()) + Double.parseDouble(yOne.getText());
        Double halfY = y/2;
        solution.setText("(" + halfX + "," + halfY + ")");
    }
}
